class CreateAnswerVariants < ActiveRecord::Migration[5.1]
  def change
    create_table :answer_variants do |t|
      t.text :body, null: false
      t.boolean :correct, default: false, null: false

      t.belongs_to :survey_question, index: true
      t.timestamps
    end
  end
end
