class UserDecorator < ApplicationDecorator
  def role
    object.role.humanize
  end

  def full_name
    [object.last_name, object.first_name, object.patronymic].join(' ')
  end

  def username_with_role
    [object.username, object.role.humanize].join(' - ')
  end

  def as_json(options = {})
    super(options).except('password_digest', 'email_confirmation').merge(
      'full_name' => full_name,
      'username_with_role' => username_with_role
    )
  end
end
