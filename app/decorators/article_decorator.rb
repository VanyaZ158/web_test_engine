class ArticleDecorator < ApplicationDecorator
  def topic_id
    object.topic.id
  end

  def body
    object.body_raw
  end

  def as_json(options = {})
    super(options).except('body_raw').merge('body' => body, 'topic_id' => topic_id)
  end
end
